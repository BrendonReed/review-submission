
# Reviews and QA

## User stories

As a customer's user, I want to view a summary of reviews so I can quickly get an idea of what reviewers have said.

As a customer's user, I want to view individual reviews so that I can get all the details.

As a customer's user, I want to view all the reviews of each rating so that I can see the general character of the good and bad reviews.

As a customer's user, I want to to review a purchased item so others can learn from my experience.

As a customer's user, I want to edit or delete a review I previously submitted.

As a customer's user, I want to prevent anyone else from editing my reviews.

As a customer's user, I want to view all my submitted content.


As a customer, I want to prevent inappropriate reviews, with as little manual
work as possible.

As a customer, I want customers to flag inappropriate content, so it can be
removed if necessary

As a customer, I want reviews that are flagged a lot to be automatically
removed from display so that the users aren't exposed to bad content

As a customer, I want reviews that are flagged to be audited so that users
aren't exposed to bad content

As a customer's user, I want to flag reviews that are inappropriate so that
content is better

As a customer's user, I want to be notified of decisions on reviews I flag so
that I know what effect my actions have.

As a customer, I want reporting on submitted reviews

## Function point analysis

### External inputs

Credentials x6
Review x4

### External outputs

Single review x5
Histogram x7
Per review/All reviews x5

### External queries

~5 x4

### Internal Logical files

2 x10

### External interface files

0

### Unadjusted FP = 67
