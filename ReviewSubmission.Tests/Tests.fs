﻿open Expecto
open System
open ReviewSubmission.AppServices
open ReviewSubmission.SubmitReview
open ReviewSubmission.Domain

let tests =
    test "A simple test" {
        let subject = "Hello World"
        Expect.equal subject "Hello World" "The strings should equal"
    }

let profanityChecker (s:string) =
    if s.Contains("hell") then
        HasProfanity { Original = s; Adjusted = s.Replace("hell", "[...]") }
    else
        None s

[<Tests>]
let simpleTest =
    testList "Tests" [
        test "ValidationFailure" {
            let r = { 
                ReviewFormDto.Rating = 6
                ReviewFormDto.Title = "profane"
                ReviewFormDto.Review = "clean"
                ReviewFormDto.ProductId = "A"
                }
            let submitResult = submitReview profanityChecker checkProfanity r
            match submitResult with
            | Result.Ok x -> Tests.failtestf "%s. Expected Ok, was Error(%A)." "" x
            | Result.Error _ -> ()
            Expect.equal 
                submitResult 
                (Error (ValidationError ["Rating must be between 1 and 5"]))
                "message" 
        }
        testCase "SubmitReview" <| fun () ->
            let r = { 
                ReviewFormDto.Rating = 5
                ReviewFormDto.Title = "clean"
                ReviewFormDto.Review = "clean"
                ReviewFormDto.ProductId = "A"
                }
            let submitResult = submitReview profanityChecker checkProfanity r
            match submitResult with
            | Result.Ok _ -> ()
            | Result.Error x -> Tests.failtestf "%s. Expected Ok, was Error(%A)." "" x
            Expect.equal 
                submitResult 
                (Ok (ReviewApproved (Clean {Rating = Rating 5;
                    Title = String50 "clean";
                    Text = ContentText "clean";
                    ProductId = String50 "A"; })))
                "message"

    ]
[<EntryPoint>]
let main args =
    //let r = runTestsWithArgs defaultConfig args tests
    let r = runTests defaultConfig simpleTest 
    Console.ReadKey() |> ignore
    r
