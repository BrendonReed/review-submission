﻿open System
open System.IO
open Suave

open Suave.Successful      // for OK-result
open Suave.Filters
open Suave.Operators
open Suave.RequestErrors
open ReviewSubmission.AppServices
open ReviewSubmission.Domain
open ReviewSubmission.SubmitReview

open Microsoft.WindowsAzure.Storage
open Microsoft.WindowsAzure.Storage.Blob

module Json =
    open Newtonsoft.Json
    
    type JsonString = string
    let serialize obj =
        JsonConvert.SerializeObject obj

    let deserialize<'a> str =
        try
            JsonConvert.DeserializeObject<'a> str
            |> Result.Ok
        with
        | ex -> Result.Error ex

let dummyprofanityChecker (s:string) =
    if s.Contains("hell") then
        HasProfanity { Original = s; Adjusted = s.Replace("hell", "[...]") }
    else
        None s

type SubmitReviewWorkflowError = 
    | DomainError of SubmitReviewError
    | DeserializationException of exn

type GetReviewError =
    | DeserializationException of exn
    | NotFound

//get json string from request
//deserialize into a'
//return Result<a'>
let deserialize<'a> req =
    let getJsonFromReq (req: HttpRequest) : Json.JsonString =
        let getString raw =
            Text.Encoding.UTF8.GetString(raw)
        req.rawForm 
        |> getString 
    getJsonFromReq req
    |> Json.deserialize

let getReviewsAzureContainer () =
    let connection = Configuration.ConfigurationManager.ConnectionStrings.["AzureBlob"].ConnectionString
    let storageAccount = CloudStorageAccount.Parse(connection)
    let blobClient = storageAccount.CreateCloudBlobClient()
    let container = blobClient.GetContainerReference("reviews")
    container

let saveDtoToBlob name id (obj) =
    let container = getReviewsAzureContainer()
    container.CreateIfNotExists() |> ignore
    let blobId = sprintf "%s%s" name id
    let blob = container.GetBlockBlobReference(blobId)
    let json = Json.serialize obj
    blob.UploadText(json)

let lookupReview id =
    let deserialize input =
        Json.deserialize<ReviewStatusDto> input
        |> Result.mapError GetReviewError.DeserializationException

    let container = getReviewsAzureContainer()
    let blobId = sprintf "%s%s" "Review" id
    let reviewM = container.GetBlobReferenceFromServer(blobId)
    if reviewM.Exists() then 
        let blob = reviewM :?> CloudBlockBlob 
        let text = blob.DownloadText()
        let dto = deserialize text 
        dto
    else 
        Result.Error GetReviewError.NotFound

let mutable approvedReviews : ReviewForm list = []
let mutable unapprovedReviews : WithProfanity list = []
//use cases
//submit review
//get all reviews for a product

let submitReview' reviewFormDto =
    reviewFormDto
    |> (ReviewSubmission.SubmitReview.submitReview dummyprofanityChecker checkProfanity)
    |> Result.tee (fun review ->
        match review with
        | ProfanityCheckedReview.Clean approved -> 
            approvedReviews <- approved :: approvedReviews
        | ProfanityCheckedReview.Detected profane -> unapprovedReviews <- profane :: unapprovedReviews
        )
    |> Result.mapError DomainError

let reviewsByProduct id = 
    let productId = (ProductId id) //todo: use smart ctor
    approvedReviews 
    |> List.filter (fun i -> i.ProductId = productId)
    |> List.map ReviewFormDto.fromDomain
    |> Result.Ok

let mapResponse result =
    match result with
    | Ok value -> OK (Json.serialize(value))
    | Error e -> (Suave.ServerErrors.INTERNAL_ERROR (Json.serialize e))

//routing
let app =
    choose [
        POST >=> choose [
            path "/review" >=> 
                request (fun req -> 
                    deserialize<ReviewFormDto> req
                    |> Result.mapError SubmitReviewWorkflowError.DeserializationException
                    |> Result.bind submitReview'
                    |> mapResponse)
        ]
        GET >=> choose [
            pathScan "/reviews/%s" (fun id ->
                warbler (fun _ -> 
                    reviewsByProduct id
                    |> mapResponse))
            pathRegex "(.*)\.(css|png|gif|js|html)" >=> Files.browseHome
        ]
        (Suave.RequestErrors.NOT_FOUND "404!")
    ]
    >=> Writers.setMimeType "text/html; charset=utf-8"

startWebServer defaultConfig app
