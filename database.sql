
create table tenant (
	id bigint not null primary key,
	tenant nvarchar(255)
	)

create table nickname (
	id bigint not null primary key,
	nickname nvarchar(255) not null,
	[location] nvarchar(255)
	)

create table review (
	id bigint not null primary key,
	tenant_id bigint foreign key references tenant(id),
	product_id nvarchar(255) not null,
	rating tinyint not null,
	title nvarchar(255) not null,
	content nvarchar(max) not null,
	[location] nvarchar(255) not null,
	nickname_id bigint not null foreign key references nickname(id)
	)