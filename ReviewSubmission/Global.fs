﻿namespace global
open System
// ======================================================
// Common code and types in the global namespace
// ======================================================


type Undefined = exn

module Scaffold =
    let notImplemented() = raise (NotSupportedException ("not implemented"))

// ======================================================
// Result
// ======================================================

/// Result represents a choice between success and failure
/// NOTE: Supplied with F# 4.1
type Result<'Success,'Failure> = 
    | Ok of 'Success 
    | Error of 'Failure


/// Functions that work with Results
module Result =

    /// Create a success case
    let success x = Ok x

    /// Create a failure case with a singleton error
    let failure  x = Error x

    /// Map over the success case
    /// signature: f:('SuccA -> 'SuccB) -> xR:Result<'SuccA,'Failure> -> Result<'SuccB,'Failure>
    let map (f:'SuccA -> 'SuccB) (xR:Result<'SuccA,'Failure>) =
        match xR with
        | Ok x -> Ok (f x)
        | Error e -> Error e 

    let tee (f:'SuccA -> 'SuccB) (xR:Result<'SuccA,'Failure>) =
        match xR with
        | Ok x -> 
            f x |> ignore
            Ok (xR)
        | Error e -> Error e 
    /// Map over the failure case
    /// signature: f:('FailureA -> 'FailureB) -> xR:Result<'Success,'FailureA> -> Result<'Success,'FailureB>
    let mapError (f:'FailureA -> 'FailureB) (xR:Result<'Success,'FailureA>) =
        match xR with
        | Ok x -> Ok x
        | Error e -> Error (f e)

    // signature: f:('SuccA -> Result<'SuccB,'Failure>) -> xR:Result<'SuccA,'Failure> -> Result<'SuccB,'Failure>
    let bind (f:'SuccA -> Result<'SuccB,'Failure>) (xR:Result<'SuccA,'Failure>)  =
        match xR with
        | Ok x -> f x
        | Error e -> Error e 

    /// Convert an Option into a Result
    let fromOption msg opt = 
        match opt with
        | Some v -> success v
        | None -> failure msg

    /// Convert the success case into an Option (useful for List.choose)
    let toOption = 
        function 
        | Ok s -> Some s
        | Error _ -> None

    /// Return a value for the failure case
    let ifFailure defaultVal = 
        function 
        | Ok x -> x
        | Error _ -> defaultVal

    /// Convert a list of Result into a Result<list> using *monadic* style. 
    /// Only the first error is returned. The error type does NOT need to be a list.
    let sequence resultList = 
        let folder result state = 
            state |> bind (fun list -> 
            result |> bind (fun element -> 
                success (element :: list)
                ))
        let initState = success []
        List.foldBack folder resultList initState 

    let (<!>) = map
// ==================================
// Result Computation expression
// ==================================
/// The `result` computation expression is available globally without qualification
[<AutoOpen>]
module ResultCE = 

    type ResultBuilder() = 
        member __.Return(x) = Result.Ok x
        member __.Bind(x, f) = Result.bind f x
    
        member __.ReturnFrom(x) = x
        member this.Zero() = this.Return ()

        member __.Delay(f) = f
        member __.Run(f) = f()

        member this.While(guard, body) =
            if not (guard()) 
            then this.Zero() 
            else this.Bind( body(), fun () -> 
                this.While(guard, body))  

        member this.TryWith(body, handler) =
            try this.ReturnFrom(body())
            with e -> handler e

        member this.TryFinally(body, compensation) =
            try this.ReturnFrom(body())
            finally compensation() 

        member this.Using(disposable:#System.IDisposable, body) =
            let body' = fun () -> body disposable
            this.TryFinally(body', fun () -> 
                match disposable with 
                    | null -> () 
                    | disp -> disp.Dispose())

        member this.For(sequence:seq<_>, body) =
            this.Using(sequence.GetEnumerator(),fun enum -> 
                this.While(enum.MoveNext, 
                    this.Delay(fun () -> body enum.Current)))

        member this.Combine (a,b) = 
            this.Bind(a, fun () -> b())

    let result = ResultBuilder()


// ======================================================
// Validated
// ======================================================

/// Validated represents a choice between success and a list of failures
type Validated<'Success,'Failure> = Result<'Success,'Failure list> 

/// Functions that work with Validated
module Validated =

    let success = Result.success

    /// Create a failure case with a list of errors
    /// This is is needed for applicative style combiners
    let failure x = Error [x]

    let map = Result.map
    let bind = Result.bind

    /// Apply a function wrapped in a Validated to a Validated value 
    /// Signature : fValidated:Validated<('SuccA -> 'SuccB),'Failure> ->  xValidated:Validated<'SuccA,'Failure> -> Validated<'SuccB,'Failure>
    let apply (fValidated:Validated<'SuccA -> 'SuccB,'Failure>) (xValidated:Validated<'SuccA,'Failure>) :Validated<'SuccB,'Failure> = 
        match fValidated, xValidated with
        | Ok f, Ok x -> Ok (f x)
        | Error errs1, Ok _ -> Error errs1
        | Ok _, Error errs2 -> Error errs2
        | Error errs1, Error errs2 -> Error (errs1 @ errs2)

    /// Lift a two parameter function to use Result parameters
    let lift2 f x1 x2 = 
        let (<!>) = map
        let (<*>) = apply
        f <!> x1 <*> x2

    /// Lift a three parameter function to use Result parameters
    let lift3 f x1 x2 x3 = 
        let (<!>) = map
        let (<*>) = apply
        f <!> x1 <*> x2 <*> x3

    /// Lift a four parameter function to use Result parameters
    let lift4 f x1 x2 x3 x4 = 
        let (<!>) = map
        let (<*>) = apply
        f <!> x1 <*> x2 <*> x3 <*> x4


    /// Convert a list of Validated into a Validated<list> using *applicative* style. 
    /// All errors will be combined. 
    let sequence (resultList:Validated<'a,'Failure> list) = 
        let cons head tail = head :: tail
        let consR = lift2 cons
        let initR = success []
        List.foldBack consR resultList initR

    let (<*>) = apply

// ======================================================
// Async
// ======================================================

[<RequireQualifiedAccess>]
module Async =

    /// Lift a value to Async
    let retn x = async.Return(x)

    /// Apply an Async function to an Async value 
    let apply fA xA = 
        async { 
         // start the two asyncs in parallel
        let! fChild = Async.StartChild fA  // run in parallel
        let! x = xA
        // wait for the result of the first one
        let! f = fChild
        return f x 
        }

    /// Apply a monadic function to an Async value  
    let bind f xA = async.Bind(xA,f)

    /// Lift a function to Async
    let map f xA = bind (f >> retn) xA

// ======================================================
// AsyncResult
// ======================================================

type AsyncResult<'Success,'Failure> = 
    Async<Result<'Success,'Failure>>

[<RequireQualifiedAccess>]
module AsyncResult =

    /// Lift a function to AsyncResult
    let map f (x:AsyncResult<_,_>) : AsyncResult<_,_> =
        Async.map (Result.map f) x

    /// Lift a value to AsyncResult
    let retn x : AsyncResult<_,_> = 
        x |> Result.success |> Async.retn

    /// Handles asynchronous exceptions and maps them into Failure cases.
    let catch f (x:AsyncResult<_,_>) : AsyncResult<_,_> =
        x
        |> Async.Catch
        |> Async.map(function
        | Choice1Of2 (Ok msg) -> Ok msg
        | Choice1Of2 (Error errors) -> Error errors
        | Choice2Of2 ex -> Error (f ex))

    /// Lift a Result into an AsyncResult
    let ofResult x : AsyncResult<_,_> = x |> Async.retn

    /// Apply an AsyncResult function to an AsyncResult value 
    let apply (fAsyncResult : AsyncResult<_, _>) (xAsyncResult : AsyncResult<_, _>) :AsyncResult<_,_> = 
        fAsyncResult |> Async.bind (fun fResult ->
        xAsyncResult |> Async.map (fun xResult -> Validated.apply fResult xResult))

    /// Apply a monadic function to an AsyncResult value  
    let bind (f: 'a -> AsyncResult<'b,'c>) (xAsyncResult : AsyncResult<_, _>) :AsyncResult<_,_> = async {
        let! xResult = xAsyncResult 
        match xResult with
            | Ok x -> return! f x
            | Error err -> return (Error err)
        }

    let ignore x = x |> bind (fun _ -> retn () )    

    /// Lift a function to transform the failures
    let mapError f (x:AsyncResult<_,_>) : AsyncResult<_,_> = 
        Async.map (Result.mapError f) x

    let ofSingleFailure x = mapError List.singleton x
    /// Create a success case
    let success x = retn x
    /// Create a success case from an Async
    let successAsync x = x |> Async.map Result.success
    /// Create a failure case
    let failure x : AsyncResult<_,_> = Async.retn (Result.failure x)

    /// Convert a list of AsyncResult into a AsyncResult<list> using monadic style. 
    /// Only the first error is returned. The error type need not be a list.
    let sequenceM resultList = 
        let folder result state = 
            state |> bind (fun list -> 
            result |> bind (fun element -> 
                success (element :: list)
                ))
        let initState = success []
        List.foldBack folder resultList initState 


/// The `asyncResult` computation expression is available globally without qualification
[<AutoOpen>]
module AsyncRopCE = 
    // ==================================
    // Computation expressions
    // ==================================
    type AsyncResultBuilder() = 
        member __.Return(x) = AsyncResult.retn x
        member __.Bind(x, f) = AsyncResult.bind f x

        member __.ReturnFrom(x) = x
        member this.Zero() = this.Return ()

        member __.Delay(f) = f
        member __.Run(f) = f()

        member this.While(guard, body) =
            if not (guard()) 
            then this.Zero() 
            else this.Bind( body(), fun () -> 
                this.While(guard, body))  

        member this.TryWith(body, handler) =
            try this.ReturnFrom(body())
            with e -> handler e

        member this.TryFinally(body, compensation) =
            try this.ReturnFrom(body())
            finally compensation() 

        member this.Using(disposable:#System.IDisposable, body) =
            let body' = fun () -> body disposable
            this.TryFinally(body', fun () -> 
                match disposable with 
                    | null -> () 
                    | disp -> disp.Dispose())

        member this.For(sequence:seq<_>, body) =
            this.Using(sequence.GetEnumerator(),fun enum -> 
                this.While(enum.MoveNext, 
                    this.Delay(fun () -> body enum.Current)))

        member this.Combine (a,b) = 
            this.Bind(a, fun () -> b())

    let asyncResult = AsyncResultBuilder()

(*
let x : Result<int,string> = Error "bad"
let f = Ok id
let x2 : Result<int,string list> = Error ["bad2"]
Validated.apply f x
Validated.apply f x2
*)

module Json =
    open Newtonsoft.Json
    
    type JsonString = string
    let serialize obj =
        JsonConvert.SerializeObject obj

    let deserialize<'a> str =
        try
            JsonConvert.DeserializeObject<'a> str
            |> Result.Ok
        with
        | ex ->
            Result.Error ex
