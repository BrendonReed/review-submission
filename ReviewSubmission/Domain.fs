﻿namespace ReviewSubmission.Domain

open System

//todo: move to private ctors
type Rating = Rating of int
type ContentText = ContentText of string
type Title = Title of string
type ProductId = ProductId of string

type ReviewForm = {
    Rating: Rating
    Title: Title
    Text: ContentText
    ProductId: ProductId
}
type WithProfanity = { Original: ReviewForm; Cleaned: ReviewForm }
type ProfanityCheckedReview = 
    | Clean of ReviewForm
    | Detected of WithProfanity

type ProfanityDetected = { Original: string; Adjusted: string }
type ProfanityChecked = 
    | None of string
    | HasProfanity of ProfanityDetected