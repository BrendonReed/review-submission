﻿namespace ReviewSubmission.AppServices
open System

type ReviewFormDto = {
    Rating: int
    Title: string
    Review: string
    ProductId: string
}

type ProfanityDetectedDto = { Original: string; Adjusted: string }

type ProfanityCheckedDto = {
    Tag: string
    HasProfanityData: ProfanityDetectedDto
}

type WithProfanityDto = { Original: ReviewFormDto; Cleaned: ReviewFormDto }

type ProfanityCheckedReviewDto = {
    Tag: string
    CleanData: ReviewFormDto
    DetectedData: WithProfanityDto
}

type ReviewStatusDto = {
    Tag: string
    ReviewApprovedData: ProfanityCheckedReviewDto 
    ReviewPendingData: ProfanityCheckedReviewDto
}

//commands
type Command<'data> = {
    Data : 'data
    Timestamp: DateTime
    UserId: string
    // etc
    }

type SubmitReviewError = 
    | ValidationError of string list

type SubmitReview = 
    Command<ReviewFormDto>
        -> Result<ReviewStatusDto, SubmitReviewError>
