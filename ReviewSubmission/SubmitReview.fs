﻿module ReviewSubmission.SubmitReview
//needed for <*> and <!> even though they're grayed out
open Result
open Validated
//////////////////////////////

open ReviewSubmission.Domain
open ReviewSubmission.AppServices

type ProfanityChecker = string -> ProfanityChecked
type CheckProfanity = 
    ProfanityChecker //dependency
        -> ReviewForm //input
        -> ProfanityCheckedReview

type SubmitReview =
    ProfanityChecker //dep for CheckProfanity
        -> CheckProfanity //dependency
        -> ReviewFormDto //input
        -> Result<ProfanityCheckedReview, SubmitReviewError>

let notImplemented() = raise (System.NotSupportedException("not implemented"))

module ProductId =
    let create (s:string) =
        //string -> Result<String50, string>
        if s <> null && s.Length > 0 && s.Length <= 50
        then Ok (ProductId s)
        else Error ["ProductId must be between 1 and 50 characters"]

module Title =
    let create (s:string) =
        //string -> Result<String50, string>
        if s <> null && s.Length > 0 && s.Length <= 50
        then Ok (Title s)
        else Error ["Title must be between 1 and 50 characters"]

module Rating =
    let create (x:int) =
        if x > 0 && x <= 5
        then Ok (Rating x)
        else Error ["Rating must be between 1 and 5"]

module ContentText =
    let create (s:string) =
        if s <> null && s.Length > 0 && s.Length <= 8000
        then Ok (ContentText s)
        else Error ["Content must be between 1 and 8000 characters"]

module ReviewFormDto =

    let createValidatedReview rating title content productId = {
        ReviewForm.Rating = rating 
        ReviewForm.Title = title 
        ReviewForm.Text = content
        ReviewForm.ProductId = productId
        }

    let fromDomain (review:ReviewForm) : ReviewFormDto =
        let (Rating rating) = review.Rating
        let (Title title) = review.Title
        let (ContentText text) = review.Text
        let (ProductId productId) = review.ProductId
        { 
            ReviewFormDto.Rating = rating
            ReviewFormDto.Title = title
            ReviewFormDto.Review = text
            ReviewFormDto.ProductId = productId
        }

    let toDomain (dto:ReviewFormDto) : Result<ReviewForm, string list> =
        let rating = Rating.create dto.Rating
        let title = Title.create dto.Title
        let content = ContentText.create dto.Review
        let productId = ProductId.create dto.ProductId
        createValidatedReview 
            <!> rating 
            <*> title 
            <*> content
            <*> productId

module WithProfanityDto =
    let fromDomain (withProfanity:WithProfanity) : WithProfanityDto = { 
        Original = ReviewFormDto.fromDomain withProfanity.Original; 
        Cleaned = ReviewFormDto.fromDomain withProfanity.Cleaned 
        }

module ProfanityCheckedReviewDto =
    let fromDomain (afterProfanityCheck:ProfanityCheckedReview) : ProfanityCheckedReviewDto =
        match afterProfanityCheck with
        | Clean r-> 
            { Tag = "Clean";
            CleanData = (ReviewFormDto.fromDomain r);
            DetectedData = Unchecked.defaultof<WithProfanityDto> }
        | Detected d -> 
            { 
                Tag = "Detected"; 
                DetectedData = 
                    { 
                    Original = ReviewFormDto.fromDomain d.Original 
                    Cleaned = ReviewFormDto.fromDomain d.Cleaned 
                    }
                CleanData = Unchecked.defaultof<ReviewFormDto> 
            }

let checkProfanity : CheckProfanity =
    fun (profanityChecker) (reviewForm: ReviewForm) ->
        let (Title title) = reviewForm.Title
        let isTitleClean = profanityChecker title
        let (ContentText reviewText) = reviewForm.Text
        let isTextClean = profanityChecker reviewText
        let result = 
            match isTitleClean with
            | None _ -> 
                match isTextClean with
                | None _ -> 
                    Domain.Clean reviewForm
                | HasProfanity profaneText -> 
                    //clean title, profane text
                    let cleaned = { reviewForm with Text = (ContentText profaneText.Adjusted) }
                    Domain.Detected ({ Original = reviewForm; Cleaned = cleaned })
            | HasProfanity profaneTitle ->
                match isTextClean with
                | None _ ->
                    //clean text, profane title
                    let cleaned = { reviewForm with Title = (Title profaneTitle.Adjusted) }
                    Domain.Detected ({ Original = reviewForm; Cleaned = cleaned })
                | HasProfanity profaneText ->
                    let cleaned = { 
                        reviewForm with 
                            Text = (ContentText profaneText.Adjusted)
                            Title = (Title profaneTitle.Adjusted) }
                    Domain.Detected ({ Original = reviewForm; Cleaned = cleaned })
        result

let submitReview : SubmitReview =
    fun profanityChecker (checkProfanity) (reviewFormDto:ReviewFormDto) ->
        let validateReview' input =
            input |> ReviewFormDto.toDomain |> Result.mapError ValidationError

        let checkProfanity' input =
            let afterProfanityCheck = input |> (checkProfanity profanityChecker)
            match afterProfanityCheck with
            | ProfanityCheckedReview.Clean _ -> Result.Ok (afterProfanityCheck)
            | ProfanityCheckedReview.Detected _ -> Result.Ok (afterProfanityCheck)

        reviewFormDto
        |> validateReview' 
        |> Result.bind checkProfanity'

